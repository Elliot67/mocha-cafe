<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<title>Mocha Café</title>
		<link rel="icon" href="img/icone.png">
		<link rel="stylesheet" type="text/css" href="styles/main.css">
		<link rel="stylesheet" type="text/css" href="styles/mBasic.css">
		<link media="screen and (min-width: 700px)" rel="stylesheet" type="text/css" href="styles/ordi.css">
		<link media="screen and (min-width: 700px)" rel="stylesheet" type="text/css" href="styles/oBasic.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
	</head>

	<body>
		<!-- TopBar -->
		<?php require('topbar.html'); ?>

		<!-- Image principale -->
		<header>
			<img src="img/logo.jpg" alt="Logo Mocha Café">
		</header>

		<!-- Menu -->
		<?php require('nav.html'); ?>

		<!-- Contenu de la page -->

		<!-- Première partie -->
		<section id="partieUne">
			<div class="imgTexte" id="grainsMobile">
				<img src="img/Cafe-En-Grain2.jpg" alt="grains de café">
				<h1><a href="#"><strong>Des Grains de Café exceptionnels</strong></a></h1>
			</div>
			<div class="imgTexte" id="grainsOrdi">
				<img src="img/Cafe-En-Grain.jpg" alt="grains de café">
				<h1><a href="#"><strong>Un café fraichement torréfié</strong></a></h1>
			</div>
			<div class="imgTexte">
				<img src="img/tasse-cafe.jpg" alt="café">
				<h1><a href="#"><strong>Nos Cafés</strong></a></h1>
			</div>
		</section>

		<hr>

		<!-- Deuxième partie -->
		<section id="partieDeux">
			<div id="slogan">
				<p><strong>Buvez un Café</strong></p>
				<p><strong>d'Exception</strong></p>
			</div>
			<div id="texte">
				<p>Fruits de variétés rares, issus de terroirs d’excellence, magnifiés par des producteurs de haut vol et de fermentations pointues, les Cafés d’Exception vous plongent dans une expérience, d’une intensité extrême.</p>
				<p>Café de méditation pour leur longeur en bouche, café de dégustation pour leur complexité ou café d’enchantement pour leur histoire. Les quantités sont cependant très réduites.</p>
				<p><a id="decouvrez" href="#">Découvrez</a></p>
			</div>
		</section>

		<hr>

		<!-- Troisième partie -->
		<section id="partieTrois">
			<div class="imgTexte">
				<img src="img/cafe-dessin.jpg" alt="">
				<h2><a href="#">L'Art du Café</a></h2>
			</div>
			<div class="imgTexte">
				<img src="img/cookies.jpg" alt="">
				<h2><a href="#">Des Cookies croquants</a></h2>
			</div>
			<div class="imgTexte">
				<img src="img/contact.jpg" alt="">
				<h2><a href="#">Contactez-nous !</a></h2>
			</div>
		</section>

		<!-- Footer -->
		<?php require('footer.html'); ?>

		<script type="text/javascript" src="js/script.js"></script>
	</body>
</html>
